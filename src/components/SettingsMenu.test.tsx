import React from 'react';
import { render, screen } from '@testing-library/react';
import { SettingsMenu } from './SettingsMenu';

test('Listen to clicks', () => {
  render(<SettingsMenu />);
  const button = screen.getByText('Bar Type');
  button.click();
  expect(button).toBeInTheDocument();
});
