import React, { Dispatch, SetStateAction } from 'react';

interface Props {
  setbarType: SetStateAction<'scatter' | 'bar' | 'arearange | boxplot'>;
  setinput: Dispatch<SetStateAction<string>>;
  types: Array<string>;
}
export const SettingsMenu = ({ setbarType, setinput, types }:Props) => {
  return (
    <div className='absolute z-[100] right-2 top-10 bg-white shadow-md rounded-md p-2 hover:cursor-pointer'>
      <div className='flex flex-col gap-y-2'>
        <div className='flex justify-between items-center'>
          <p className='text-xs'>Bar Type</p>
          <select
            className='border border-gray-200 rounded-md p-1'
            onChange={(e) => setbarType(e.target.value as Props['setbarType'])}>
            {types.map((item) => (
              <option key={item} value={item}>
                {item === 'arearange'
                  ? 'Area Range'
                  : item.charAt(0).toUpperCase() + item.slice(1)}
              </option>
            ))}
          </select>
        </div>
        <div className='flex justify-between items-center'>
          <p className='text-xs mr-2'>Title</p>
          <input
            type='text'
            className='border border-gray-200 rounded-md p-1'
            onChange={(e) => setinput(e.target.value)}
          />
        </div>
      </div>
    </div>
  );
};
