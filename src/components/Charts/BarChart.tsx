import * as React from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { GrMenu } from 'react-icons/gr';
import { SettingsMenu } from '../SettingsMenu';

interface IAreaRangeChartProps {}
interface stateProps {
  input: string;
  barType: 'scatter' | 'bar';
  showdropdown: boolean;
}
export function BarChart(props: IAreaRangeChartProps) {
  const [input, setinput] = React.useState<stateProps['input']>('Bar Chart');
  const [barType, setbarType] = React.useState<stateProps['barType']>(
    'bar'
  );
  const [showdropdown, setshowdropdown] = React.useState<stateProps["showdropdown"]>(false);
  const types = ['scatter', 'bar'];

  const option = {
    chart: {
      type: barType,
    },
    title: {
      text: input,
      align: 'left',
    },
    xAxis: {
      categories: ['Africa', 'America', 'Asia', 'Europe'],
      title: {
        text: null,
      },
      gridLineWidth: 1,
      lineWidth: 0,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Population (millions)',
        align: 'high',
      },
      labels: {
        overflow: 'justify',
      },
      gridLineWidth: 0,
    },
    tooltip: {
      valueSuffix: ' millions',
    },
    plotOptions: {
      bar: {
        borderRadius: '50%',
        dataLabels: {
          enabled: true,
        },
        groupPadding: 0.1,
      },
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor: '#FFFFFF',
      shadow: true,
    },
    credits: {
      enabled: false,
    },
    series: [
      {
        name: 'Year 1990',
        data: [631, 727, 3202, 721],
      },
      {
        name: 'Year 2000',
        data: [814, 841, 3714, 726],
      },
      {
        name: 'Year 2018',
        data: [1276, 1007, 4561, 746],
      },
    ],
  };

  return (
    <div className='relative'>
      <div
        className='absolute z-[90] right-2 top-2'
        onClick={() => setshowdropdown(!showdropdown)}>
        <GrMenu color='black' size='1.2rem' className='' />
      </div>
      {showdropdown && (
        <SettingsMenu
          setbarType={setbarType}
          setinput={setinput}
          types={types}
        />
      )}
      <HighchartsReact
        options={option}
        highcharts={Highcharts}
        className='w-20 h-20 z-50'
      />
    </div>
  );
}
