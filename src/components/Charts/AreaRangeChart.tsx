import * as React from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { GrMenu } from 'react-icons/gr';
import { SettingsMenu } from '../SettingsMenu';

interface IAreaRangeChartProps {}
interface stateProps {
  input: string;
  barType: 'scatter' | 'bar' | 'arearange';
  showdropdown: boolean;
}

export function AreaRangeChart(props: IAreaRangeChartProps) {
  const [input, setinput] =
  React.useState<stateProps['input']>('Area Range Chart');
  const [barType, setbarType] =
  React.useState<stateProps['barType']>('arearange');
  const [showdropdown, setshowdropdown] =
  React.useState<stateProps['showdropdown']>(false);
  const types = ['scatter', 'bar', 'arearange']

  function getData(n: number) {
    const arr = [];
    let a: number;
    let b: number;
    let c: number;
    let low: number;
    let spike: number;
    for (let i = 0; i < n; i = i + 1) {
      if (i % 100 === 0) {
        a = 2 * Math.random();
      }
      if (i % 1000 === 0) {
        b = 2 * Math.random();
      }
      if (i % 10000 === 0) {
        c = 2 * Math.random();
      }
      if (i % 50000 === 0) {
        spike = 10;
      } else {
        spike = 0;
      }
      low = 2 * Math.sin(i / 100) + a + b + c + spike + Math.random();
      arr.push([i, low, low + 5 + 5 * Math.random()]);
    }
    return arr;
  }
  const n = 5000,
    data = getData(n);

  const option2 = {
    title: {
      text: input,
    },
    type: barType,
    zoomType: 'x',
    panning: true,
    panKey: 'shift',
    xAxis: {
      crosshair: true,
    },
    tooltip: {
      valueDecimals: 2,
    },
    series: [
      {
        data: data,
      },
    ],
  };

  return (
    <div className='relative'>
      <div
        className='absolute z-[90] right-2 top-2'
        onClick={() => setshowdropdown(!showdropdown)}>
        <GrMenu color='black' size='1.2rem' className='' />
      </div>
      {showdropdown && (
        <SettingsMenu setbarType={setbarType} setinput={setinput} types={types} />
      )}
      <HighchartsReact
        options={option2}
        highcharts={Highcharts}
        className='w-20 h-20 z-50'
      />
    </div>
  );
}
