import * as React from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { GrMenu } from 'react-icons/gr';
import { SettingsMenu } from '../SettingsMenu';

interface IScatterChartProps {}
interface stateProps {
  input: string;
  barType: 'scatter' | 'bar';
  showdropdown: boolean;
}
export function ScatterChart(props: IScatterChartProps) {
  const [input, setinput] =
    React.useState<stateProps['input']>('Scatter Chart');
  const [barType, setbarType] =
    React.useState<stateProps['barType']>('scatter');
  const [showdropdown, setshowdropdown] =
    React.useState<stateProps['showdropdown']>(false);
  const types = ['scatter', 'bar'];

  const arrayRange = (start: number, stop: number, step: number) =>
    Array.from(
      { length: (stop - start) / step + 1 },
      (value, index) => start + index * step
    );
  const arrayLength: number[] = arrayRange(1, 50, 1);
  const data: number[] = [];
  arrayLength.map((item: number) => {
    return data.push(1 * Math.random());
  });
  const option2 = {
    title: {
      text: input,
    },
    series: [
      {
        type: barType,
        data,
      },
    ],
  };
  return (
    <div className='relative'>
      <div
        className='absolute z-[90] right-2 top-2'
        onClick={() => setshowdropdown(!showdropdown)}>
        <GrMenu color='black' size='1.2rem' className='' />
      </div>
      {showdropdown && (
        <SettingsMenu
          setbarType={setbarType}
          setinput={setinput}
          types={types}
        />
      )}
      <HighchartsReact
        options={option2}
        highcharts={Highcharts}
        className='w-20 h-20 z-50'
      />
    </div>
  );
}

Highcharts.setOptions({
  colors: ['rgba(5,141,199,0.5)', 'rgba(80,180,50,0.5)', 'rgba(237,86,27,0.5)'],
});
