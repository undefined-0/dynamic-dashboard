import * as React from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { GrMenu } from 'react-icons/gr';
import { SettingsMenu } from '../SettingsMenu';

interface IBoxWhiskerPlotChartProps {}
interface stateProps {
  input: string;
  barType: 'scatter' | 'bar' | 'arearange' | 'boxplot';
  showdropdown: boolean;
}
export function BoxWhiskerPlotChart(props: IBoxWhiskerPlotChartProps) {
  const [input, setinput] = React.useState<stateProps['input']>(
    'Box and Whisker Plot'
  );
  const [barType, setbarType] =
    React.useState<stateProps['barType']>('boxplot');
  const [showdropdown, setshowdropdown] =
    React.useState<stateProps['showdropdown']>(false);
  const types = ['scatter', 'bar', 'arearange', 'boxplot'];

  const arrayRange = (start: number, stop: number, step: number) =>
    Array.from(
      { length: (stop - start) / step + 1 },
      (value, index) => start + index * step
    );
  const arrayLength: number[] = arrayRange(1, 50, 1);
  const data: number[] = [];
  arrayLength.map((item: number) => {
    return data.push(1 * Math.random());
  });
  const option2 = {
    title: {
      text: input,
    },
    type: barType,
    series: [
      {
        name: 'Observations',
        data: [
          [760, 801, 848, 895, 965],
          [733, 853, 939, 980, 1080],
          [714, 762, 817, 870, 918],
          [724, 802, 806, 871, 950],
          [834, 836, 864, 882, 910],
        ],
        tooltip: {
          headerFormat: '<em>Experiment No {point.key}</em><br/>',
        },
      },
      {
        name: 'Outliers',
        color: Highcharts.getOptions().colors[0],
        type: 'scatter',
        data: [
          // x, y positions where 0 is the first category
          [20, 644],
          [309, 718],
          [400, 951],
          [809, 969],
        ],
        marker: {
          fillColor: 'white',
          lineWidth: 1,
          lineColor: Highcharts.getOptions().colors[0],
        },
        tooltip: {
          pointFormat: 'Observation: {point.y}',
        },
      },
    ],
  };
  return (
    <div className='relative'>
      <div
        className='absolute z-[90] right-2 top-2'
        onClick={() => setshowdropdown(!showdropdown)}>
        <GrMenu color='black' size='1.2rem' className='' />
      </div>
      {showdropdown && (
        <SettingsMenu
          setbarType={setbarType}
          setinput={setinput}
          types={types}
        />
      )}
      <HighchartsReact
        options={option2}
        highcharts={Highcharts}
        className='w-20 h-20 z-50'
      />
    </div>
  );
}
