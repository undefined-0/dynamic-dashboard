import * as React from 'react';
import { BoxWhiskerPlotChart } from '../Charts/BoxWiskerPlot';
import { AreaRangeChart } from '../Charts/AreaRangeChart';
import { ScatterChart } from '../Charts/ScatterChart';
import GridLayout from 'react-grid-layout';
import { BarChart } from '../Charts/BarChart';

export interface IDashboardProps {}
export function Dashboard(props: IDashboardProps) {
  
  const layout = [
    { i: 'a', x: 0, y: 0, w: 1, h: 10 },
    { i: 'b', x: 1, y: 0, w: 1, h: 10 },
    { i: 'c', x: 0, y: 0, w: 1, h: 10 },
    { i: 'd', x: 1, y: 0, w: 1, h: 10 },
  ];
  return (
    <GridLayout
      className='layout'
      layout={layout}
      cols={2}
      rowHeight={32}
      width={1200}>
      <div key='a' className='bg-white'>
        <BarChart />
      </div>
      <div key='b' className='bg-white'>
        <BoxWhiskerPlotChart />
      </div>
      <div key='c' className='bg-white'>
        <ScatterChart />
      </div>
      <div key='d' className='bg-white'>
        <AreaRangeChart />
      </div>
    </GridLayout>
  );
}
