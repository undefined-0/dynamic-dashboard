import * as React from "react";
import userImg from "../../assets/image.jpeg";
import { FiMessageCircle } from "react-icons/fi";

export interface ILayoutProps {
  children?: React.ReactNode;
}
export function Layout({ children }: ILayoutProps) {
  return (
    <div className="bg-zinc-100 w-screen min-h-screen font-lato pt-40">
      <nav className="bg-white py-4 px-10 flex justify-between rounded-xl fixed top-4 w-[95%] left-[2.5%] ">
        <h1 className="my-auto">Astra FE coding assessment</h1>
        <div className="flex gap-x-4">
          <img src={userImg} alt="user_img" className="w-8 h-8 rounded-full" />
          <div className="bg-white rounded-full w-8 h-8 shadow-md relative flex justify-center items-center border border-gray-50 ">
            <FiMessageCircle color="black" size="1.2rem" className="" />
            <div className="w-2 h-2 bg-red-600 rounded-full absolute top-0 right-0" />
          </div>
        </div>
      </nav>
      <div className="w-[98%] max-w-[80rem] mx-auto h-fit pb-10">{children}</div>
    </div>
  );
}
