import "./App.css";
import { Layout } from "./components/templates/layout";
import { Dashboard } from "./components/templates/DashBoard";

function App() {

  return (
    <div className="">
      <Layout>
        <Dashboard />
      </Layout>
    </div>
  );
}

export default App;
