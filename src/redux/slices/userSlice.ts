import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  user: {
    username: 'Jaypee',
    email: 'jaypee@gmail.com',
    image:
      '',
  },
}

export const userSlice: any = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state: any, action) => {
      state.user = action.payload
    },
    logout: (state: any) => {
      state.user = undefined;
    },
  },
})
export const { logout, setUser } = userSlice.actions
export const selectUser = (state: any) => state.user.user
export default userSlice.reducer
