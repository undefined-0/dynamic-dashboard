/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    screens: {
      mobile: "1px",
      tablet: "500px",
      tabletM: "700px",
      desktop: "1024px",
    },
    fontSize: {
      xxxs: "6px",
      xxs: "10px",
      xs: "12px",
      sm: "16px",
      md: "20px",
      lg: "24px",
      xl: "32px",
      "2xl": "44px",
      "3xl": "60px",
      "4xl": "2.441rem",
      "5xl": "3.052rem",
    },
    extend: {
      fontFamily: {
        lato: ["Lato, serif"],
      },
      colors: {
        mainBrown: "#763D24",
        textGrey: "#5A5A5A",
        fadedGreen: "#8B822B",
        brown2: "#8D543B",
        bgGrey: "#efwefef",
      },
    },
  },
  plugins: [],
};
