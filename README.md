# Running the project locally

This project is my solution to an assessment project by Astra Innovations. The project is a simple web app that displays a grid of charts. The project is built using React and Typescript.

## `Getting Started`

These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.\

### Installing

Follow these steps to get the project running on your local machine:

1. Clone the repository to your local machine:

```bash
git clone https://gitlab.com/jaypee-0/dynamic-dashboard.git
```

2. Change into the project directory:

```bash
cd dynamic-dashboard
```

3. Install project dependencies using npm:

```bash
npm install
```

4. To start a development server and run the project locally, run the following command:

```bash
npm start
```

5. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
